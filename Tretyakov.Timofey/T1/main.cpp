#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <bitset>
#include <iomanip>
#include <sstream>
#include <iterator>

struct DataStruct {
    double key1;
    unsigned long long key2;
    std::string key3;
};

std::istream& operator>>(std::istream& is, DataStruct& data) {
    char c;
    while (is >> c) {
        if (c == ':') {
            std::string key;
            is >> key;
            if (key == "key1") {
                double value;
                if (is >> value) {
                    data.key1 = value;
                }
                else {
                    return is;
                }
            }
            else if (key == "key2") {
                std::string value_str;
                getline(is, value_str, ':');
                is.putback(':');
                unsigned long long value;
                if (value_str.size() > 2 && value_str[1] == '0' && (value_str[2] == 'b' || value_str[2] == 'B')) {
                    value = std::bitset<64>(value_str.substr(3)).to_ullong();
                    data.key2 = value;
                }
                else {
                    return is;
                }
            }
            else if (key == "key3") {
                std::string value;
                if (is >> c && c == '"' && getline(is, value, '"')) {
                    data.key3 = value;
                }
                else {
                    return is;
                }
            }
            else {
                break;
            }
        }
        else if (c == ')') {
            break;
        }
    }
    return is;
}

bool operator<(const DataStruct& d1, const DataStruct& d2) {
    if (d1.key1 < d2.key1) {
        return true;
    }
    else if (d1.key1 == d2.key1) {
        if (d1.key2 < d2.key2) {
            return true;
        }
        else if (d1.key2 == d2.key2) {
            return d1.key3.length() < d2.key3.length();
        }
    }
    return false;
}

std::string double_to_scientific_string(double value) {
    std::ostringstream ss;
    ss << std::scientific << std::setprecision(2) << value;
    std::string str = ss.str();
    if (str[0] == '-') {
        str.erase(0, 1);
    }
    if (str[0] == '0') {
        str.erase(0, 1);
    }
    return str;
}

std::string ull_to_binary_string(unsigned long long value) {
    std::ostringstream ss;
    ss << "0b" << std::bitset<sizeof(unsigned long long) * 8>(value).to_string();
    return ss.str();
}

std::ostream& operator<<(std::ostream& os, const DataStruct& d) {
    os << "(key1 " << double_to_scientific_string(d.key1) << ":key2 '" << ull_to_binary_string(d.key2) << "':key3 \"" << d.key3 << "\":)";
    return os;
}

template<typename InputIt>
void read_from_istringstream(std::istringstream& iss, InputIt begin, InputIt end)
{
    while (begin != end && iss >> *begin) {
        ++begin;
    }
}

int main() {
    std::string input_string = "(:key1 5.45e-2:key2 0b1000101:key3 \"Data\":)\n(:key3 \"Data\":key2 0b1000101:key1 4.45e-2:)\n(:key1 5.45e-2:key2 0b1000001:key3 \"Data\":)";
    std::istringstream iss(input_string);

    std::vector<DataStruct> data;
    std::copy(std::istream_iterator<DataStruct>(iss), std::istream_iterator<DataStruct>(), std::back_inserter(data));

    std::sort(data.begin(), data.end());
    std::ostream_iterator<DataStruct> out_it(std::cout, "\n");
    std::copy(data.begin(), data.end(), out_it);
    return 0;
}
